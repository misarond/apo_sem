#include "logic.h"
#include "utils.h"
#include <stdbool.h>

void logic(int step, bool player2, Snake *snake)
{
    if (snake->dir != STOP)
    { // snake is moving
        for (int i = snake->len - 1; i > 0; --i)
        {
            snake->tail_arr_x[i] = snake->tail_arr_x[i - 1];
            snake->tail_arr_y[i] = snake->tail_arr_y[i - 1];
        }
        // first piece of the tail behind head
        snake->tail_arr_x[0] = snake->head_x;
        snake->tail_arr_y[0] = snake->head_y;
        // last piece of the snake
        snake->tail_x = snake->tail_arr_x[snake->len - 1];
        snake->tail_y = snake->tail_arr_y[snake->len - 1];
    }

    head_move(snake);

    borders_check(snake, player2);

    fruit_check(snake, player2, step);

    hit_body_check(snake, player2);

    second_player_hit_check(snake, player2);

    if (!game.game_over && !game.level_done)
    {
        draw();
    }
}

// - function -----------------------------------------------------------------
void score_change(bool player2)
{
    int score;
    int x;
    if (player2)
    {
        x = 136;
        score = snake_player2.score;
    }
    else
    {
        x = 388;
        score = snake_player.score;
    }
    char score_str[6];
    int_to_str(score, score_str);
    int i = 0;
    while (score_str[i] == '0' && i < 4)
    {
        ++i;
    }
    int j = 0;
    while (score_str[i] != '\0')
    {
        font_print(score_str[i], true, x + j * 12, 290, false, 255, 0, 144);
        ++i;
        ++j;
    }

    score += 10;

    int_to_str(score, score_str);
    i = 0;
    while (score_str[i] == '0')
    {
        ++i;
    }
    j = 0;
    while (score_str[i] != '\0')
    {
        font_print(score_str[i], true, x + j * 12, 290, true, 255, 0, 144);
        ++i;
        ++j;
    }
    if (player2)
    {
        snake_player2.score = score;
    }
    else
    {
        snake_player.score = score;
    }
}

// - function -----------------------------------------------------------------
void int_to_str(int value, char *str)
{
    for (int i = 0; i < 5; ++i)
    {
        str[4 - i] = value % 10 + '0';
        value = value / 10;
    }
    str[5] = '\0';
}

// - function -----------------------------------------------------------------
void head_move(Snake *snake)
{
    switch (snake->dir)
    {
    case LEFT:
        snake->head_x -= 3;
        break;
    case RIGHT:
        snake->head_x += 3;
        break;
    case UP:
        snake->head_y -= 3;
        break;
    case DOWN:
        snake->head_y += 3;
        break;
    default:
        break;
    }
}

// - function -----------------------------------------------------------------
void borders_check(Snake *snake, bool player2)
{
    if (snake->head_x >= RIGHT_BORDER || snake->head_x <= LEFT_BORDER || snake->head_y >= DOWN_BORDER || snake->head_y <= UP_BORDER)
    {
        if (!player2)
        { // Player1 snake hits a border
            snake_player.crash = true;
            game.game_over = true;
            // switch led to red
            *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB2_o) = RED_LED;
        }
        else
        { // Player2 snake hits a border
            snake_player2.crash = true;
            game.level_done = true;
            *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB1_o) = RED_LED;
        }
    }
}

// - function -----------------------------------------------------------------
void fruit_check(Snake *snake, bool player2, int step)
{
    if ((snake->head_x == game.fruit_x && snake->head_y == game.fruit_y))
    {
        if (player2)
        {
            *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB1_o) = BLUE_LED; // switch led to blue
            score_change(true);
        }
        else
        {
            *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB2_o) = BLUE_LED;
            score_change(false);
        }
        *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_LINE_o) = (snake_player2.score + snake_player.score) / 10; // score in led line

        game.fruit_x = (((step * game.fruit_x * 723) % 157) * 3) + 4; // pseudo random spawn fruit
        game.fruit_y = (((step * 546 * game.fruit_y) % 81) * 3) + 34;

        snake->len += 1;
        if (snake->len == 100 && !player2)
        { // Player1 snake hits 1000 score
            snake_player2.crash = true;
            game.level_done = true;
        }
        else if (snake->len == 100)
        { // Player2 snake hits 1000 score
            snake_player.crash = true;
            game.game_over = true;
        }
    }
}

// - function -----------------------------------------------------------------
void hit_body_check(Snake *snake, bool player2)
{
    for (int i = 0; i < snake->len; ++i)
    {
        if (snake->head_x == snake->tail_arr_x[i] && snake->head_y == snake->tail_arr_y[i])
        {
            if (!player2)
            {
                game.game_over = true;
                snake_player.crash = true;
                *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB2_o) = RED_LED;
            }
            else
            {
                game.game_over = true;
                snake_player2.crash = true;
                *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB1_o) = RED_LED;
            }
        }
    }
}

// - function -----------------------------------------------------------------
void second_player_hit_check(Snake *snake, bool player2)
{
    if (!player2)
    { // Player1 snake
        if (snake->head_x == snake_player2.head_x && snake->head_y == snake_player2.head_y)
        {
            snake_player.crash = true;
            game.game_over = true;
            *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB2_o) = RED_LED;
        }
        for (int i = 0; i < snake_player2.len; ++i)
        {
            if (snake->head_x == snake_player2.tail_arr_x[i] && snake->head_y == snake_player2.tail_arr_y[i])
            {
                snake_player.crash = true;
                game.game_over = true;
                *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB2_o) = RED_LED;
            }
        }
    }
    else
    { // Player2 snake
        if (snake->head_x == snake_player.head_x && snake->head_y == snake_player.head_y)
        {
            snake_player2.crash = true;
            game.game_over = true;
            *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB1_o) = RED_LED;
        }
        for (int i = 0; i < snake_player.len; ++i)
        {
            if (snake->head_x == snake_player.tail_arr_x[i] && snake->head_y == snake_player.tail_arr_y[i])
            {
                snake_player2.crash = true;
                game.game_over = true;
                *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB1_o) = RED_LED;
            }
        }
    }
}
// - function -----------------------------------------------------------------
void draw(void)
{
    volatile uint16_t *lcd = (volatile uint16_t *)LCD_FB_START;
    for (int i = 0; i < 3; i++)
    {
        lcd[snake_player.tail_x + snake_player.tail_y * LCD_WIDTH - LCD_WIDTH - 1 + i] = make_pixel(0, 0, 0);
        lcd[snake_player.tail_x + snake_player.tail_y * LCD_WIDTH - 1 + i] = make_pixel(0, 0, 0);
        lcd[snake_player.tail_x + snake_player.tail_y * LCD_WIDTH + LCD_WIDTH - 1 + i] = make_pixel(0, 0, 0);

        lcd[snake_player.head_x + snake_player.head_y * LCD_WIDTH - LCD_WIDTH - 1 + i] = make_pixel(255, 255, 255);
        lcd[snake_player.head_x + snake_player.head_y * LCD_WIDTH - 1 + i] = make_pixel(255, 255, 255);
        lcd[snake_player.head_x + snake_player.head_y * LCD_WIDTH + LCD_WIDTH - 1 + i] = make_pixel(255, 255, 255);

        lcd[snake_player2.tail_x + snake_player2.tail_y * LCD_WIDTH - LCD_WIDTH - 1 + i] = make_pixel(0, 0, 0);
        lcd[snake_player2.tail_x + snake_player2.tail_y * LCD_WIDTH - 1 + i] = make_pixel(0, 0, 0);
        lcd[snake_player2.tail_x + snake_player2.tail_y * LCD_WIDTH + LCD_WIDTH - 1 + i] = make_pixel(0, 0, 0);

        lcd[snake_player2.head_x + snake_player2.head_y * LCD_WIDTH - LCD_WIDTH - 1 + i] = make_pixel(0, 0, 255);
        lcd[snake_player2.head_x + snake_player2.head_y * LCD_WIDTH - 1 + i] = make_pixel(0, 0, 255);
        lcd[snake_player2.head_x + snake_player2.head_y * LCD_WIDTH + LCD_WIDTH - 1 + i] = make_pixel(0, 0, 255);

        lcd[game.fruit_x + game.fruit_y * LCD_WIDTH - LCD_WIDTH - 1 + i] = make_pixel(0, 255, 0);
        lcd[game.fruit_x + game.fruit_y * LCD_WIDTH - 1 + i] = make_pixel(0, 255, 0);
        lcd[game.fruit_x + game.fruit_y * LCD_WIDTH + LCD_WIDTH - 1 + i] = make_pixel(0, 255, 0);
    }
    *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB1_o) = GREEN_LED; // switch led to green
    *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_LED_RGB2_o) = GREEN_LED;
}