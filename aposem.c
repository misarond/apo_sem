//------------------------
// B35APO Semestral work
// Matej Misar, Ondrej Misar
// 05.06.2020
// --------Snake----------

#define _POSIX_C_SOURCE 200112L

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "font_types.h"
#include "logic.h"
#include "utils.h"
#include "menu.h"
#include "setup.h"
#include "ai.h"
#include "over.h"
#include "menu_text.h"
#include "input.h"

void input(void);

// - main -----------------------------------------------------------------
int main(int argc, char *argv[])
{
    unsigned char *mem_base = (unsigned char *)SPILED_REG_BASE;
    volatile uint16_t *lcd = (volatile uint16_t *)LCD_FB_START;
    uint8_t inp_dir, inp_dir_blue;
    uint32_t knobs;

    while (!menu_str.end_menu)
    {
        knobs = *(volatile uint32_t *)(mem_base + SPILED_REG_KNOBS_8BIT_o);
        inp_dir_blue = knobs;
        inp_dir = knobs >> 8;
        click(inp_dir_blue);   //calls function in menu.h
        logic_menu();          //calls function in menu.h
        menu(lcd);             //calls function in menu.h
        control_menu(inp_dir); //calls function in menu.h
        pointer(lcd);          //calls function in menu.h
    }

    setup();
    int step = 0; // for pseudo random generating fruit

    int speed;
    if (menu_str.speed == 1)
    {
        speed = 300;
    }
    else if (menu_str.speed == 2)
    {
        speed = 100;
    }
    else
    {
        speed = 0;
    }

    while (!game.game_over && !game.level_done)
    {
        ++step;
        input();
        if (menu_str.AI){
            ai();
        }

        logic(step, false, &snake_player);
        logic(step, true, &snake_player2);

        volatile int temp;
        if (!menu_str.AI)
        {
            for (int i = 0; i < speed; ++i) // slowing mechanism
            {
                ++temp;
            }
        }
    }
    game_over(); //calls function in over.h
    winner();

    return 0;
}
