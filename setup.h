#ifndef __SETUP_H__
#define __SETUP_H__

#include <stdint.h>
#include "utils.h"

// main setup function
void setup(void);

// arena borders draw function
void make_borders(volatile uint16_t *lcd);

// game and snake structs initialization
void snake_game_init(Snake *snake_player1, Snake *snake_player2, Game *game);


#endif