#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdint.h>
#include <stdbool.h>

#define SERIAL_PORT_BASE 0xffffc000

#define SERP_RX_ST_REG_o 0x00
#define SERP_RX_ST_REG_READY_m 0x1
#define SERP_RX_ST_REG_IE_m 0x2
#define SERP_RX_DATA_REG_o 0x04

#define LCD_FB_START 0xffe00000
#define LCD_FB_END 0xffe4afff
#define LCD_WIDTH 480
#define LCD_HEIGHT 320

#define SPILED_REG_BASE 0xffffc100

#define SPILED_REG_LED_LINE_o 0x004
#define SPILED_REG_LED_RGB1_o 0x010
#define SPILED_REG_LED_RGB2_o 0x014
#define SPILED_REG_LED_KBDWR_DIRECT_o 0x018

#define SPILED_REG_KBDRD_KNOBS_DIRECT_o 0x020
#define SPILED_REG_KNOBS_8BIT_o 0x024

#define RED_LED 0x00ff0000
#define GREEN_LED 0x0000ff00
#define BLUE_LED 0x000000ff
#define YELLOW 255
#define ORANGE 165

#define UP_BORDER 31
#define DOWN_BORDER 280
#define LEFT_BORDER 1
#define RIGHT_BORDER 478

typedef enum
{
    STOP = 0,
    LEFT,
    UP,
    RIGHT,
    DOWN
} Direction;

typedef struct
{
    uint16_t head_x;
    uint16_t head_y;
    int len;
    uint16_t tail_arr_x[100];
    uint16_t tail_arr_y[100];
    uint16_t tail_x;
    uint16_t tail_y;
    Direction dir;
    int score;
    bool crash;
} Snake;

typedef struct
{
    bool game_over;
    bool level_done;
    int fruit_x;
    int fruit_y;
    bool big_font;
} Game;

Snake snake_player;
Snake snake_player2;
Game game;

// creates color pixel from input values
uint16_t make_pixel(uint8_t r, uint8_t g, uint8_t b);

// prints char on lcd with specified font, color and cords
void font_print(char c, bool big_font, int cord_x, int cord_y, bool print, uint8_t r, uint8_t g, uint8_t b);

#endif