#include "over.h"
#include "menu.h"
#include "utils.h"

//fction prints Game over when snake hits border or his body
void game_over(void)
{
    int px = 14;
    char game_over[10] = "GAME OVER";
    for (int i = 0; i < 10; i++)
    {
        font_print(game_over[i], true, 177 + i * px, 150, true, 255, 0, 0);
    }
}

void winner(void)
{
    char *winner;
    int len, r, g;
    int compensation = 0;
    if (snake_player.crash)
    {
        winner = "BLUE WINS";
        len = 9;
        r = g = 0;
    }
    else
    {
        winner = "WHITE WINS";
        len = 10;
        compensation = 7;
        r = g = 255;
    }
    for (int i = 0; i < len; i++)
    {
        font_print(winner[i], true, 177 + i * 14 - compensation, 169, true, r, g, 255);
    }
}
