#include "input.h"
#include "utils.h"
#include "menu.h"
#include <stdbool.h>
#include <stdint.h>

void input(void){
    
    /*
    *   Knobs controll
    * 
    uint8_t inp_dir = *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_KNOBS_8BIT_o) >> 16;
    uint8_t inp_dir_blue = *(volatile uint32_t *)(SPILED_REG_BASE + SPILED_REG_KNOBS_8BIT_o);
    if (inp_dir > 32 && inp_dir < 96 && snake_player.dir != RIGHT)
    {
        snake_player.dir = LEFT;
    }
    else if (inp_dir >= 96 && inp_dir < 160 && snake_player.dir != DOWN)
    {
        snake_player.dir = UP;
    }
    else if (inp_dir >= 160 && inp_dir < 224 && snake_player.dir != LEFT)
    {
        snake_player.dir = RIGHT;
    }
    else if (snake_player.dir != UP)
    {
        snake_player.dir = DOWN;
    }
    */

    // Terminal controll
    uint32_t inp = *(volatile uint32_t *)(SERIAL_PORT_BASE + 4);
    char c = inp;
    if (c == 'a' && snake_player.dir > 1)
    {
        --snake_player.dir;
    }
    else if (c == 'a')
    {
        snake_player.dir = DOWN;
    }
    else if (c == 's' && snake_player.dir < 4)
    {
        ++snake_player.dir;
    }
    else if (c == 's')
    {
        snake_player.dir = LEFT;
    }

    if (!menu_str.AI)
    { // two players option

        // terminal input
        if (c == 'k' && snake_player2.dir > 1)
        {
            --snake_player2.dir;
        }
        else if (c == 'k')
        {
            snake_player2.dir = DOWN;
        }
        else if (c == 'l' && snake_player2.dir < 4)
        {
            ++snake_player2.dir;
        }
        else if (c == 'l')
        {
            snake_player2.dir = LEFT;
        }

        // blue knob input
        /*
        *   Knobs controll
        * 
        if (inp_dir_blue > 32 && inp_dir_blue < 96 && snake_player2.dir != RIGHT)
        {
            snake_player2.dir = LEFT;
        }
        else if (inp_dir_blue >= 96 && inp_dir_blue < 160 && snake_player2.dir != DOWN)
        {
            snake_player2.dir = UP;
        }
        else if (inp_dir_blue >= 160 && inp_dir_blue < 224 && snake_player2.dir != LEFT)
        {
            snake_player2.dir = RIGHT;
        }
        else if (snake_player2.dir != UP)
        {
            snake_player2.dir = DOWN;
        }
        */
    }
}