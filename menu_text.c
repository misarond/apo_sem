#include "menu.h"
#include "utils.h"

//function - prints/clears/updates menu text
void menu(volatile uint16_t *lcd)
{
    bool font_bold;
    char cross;
    if ((menu_str.AI && menu_str.AI_change == false) || (!menu_str.AI && menu_str.AI_change == true))
    {
        cross = 'Y';
    }
    else
    {
        cross = 'N';
    }
    if (menu_str.clear && menu_str.font_change)
    {

        font_bold = !menu_str.bold_font;
        menu_str.font_change = false;
    }
    else
    {
        font_bold = menu_str.bold_font;
    }

    int px;
    int extra_width;
    if (font_bold)
    {
        px = 14;
        extra_width = 120;
    }
    else
    {
        px = 8;
        extra_width = 80;
    }
    if (!menu_str.refresh)
    {
        if (menu_str.speed_ref)
        {
            for (int i = 0; i < 3; i++)
            {
                font_print('|', font_bold, extra_width + i * px, 100, false, 255, YELLOW, 0);
            }
            for (int i = 0; i < menu_str.speed; i++)
            {
                font_print('|', font_bold, extra_width + i * px, 100, true, 255, YELLOW, 0);
            }
            menu_str.speed_ref = false;
        }
        if (menu_str.AI_change)
        {
            if (font_bold)
            {
                font_print(cross, font_bold, 70, 180, false, 255, ORANGE, 0);
            }
            else
            {
                font_print(cross, font_bold, 58, 180, false, 255, ORANGE, 0);
            }
            menu_str.refresh = true;
            menu_str.AI_change = false;
        }
    }
    else
    {

        bool print = true;
        if (menu_str.clear)
        {
            print = false;
        }
        if (menu_str.end_menu == true)
        {
            print = false;
        }
        char name[6] = "SNAKE";
        for (int i = 0; i < 6; i++)
        {
            font_print(name[i], font_bold, 30 + i * px, 20, print, 200, 255, 120);
        }
        char play[5] = "PLAY";
        for (int i = 0; i < 5; i++)
        {
            if (i > 1)
            {
                font_print(play[i], font_bold, 27 + i * px, 60, print, 255, YELLOW, 0);
            }
            else
            {
                font_print(play[i], font_bold, 30 + i * px, 60, print, 255, YELLOW, 0);
            }
        }
        char speed[7] = "SPEED";
        for (int i = 0; i < 7; i++)
        {
            font_print(speed[i], font_bold, 30 + i * px, 100, print, 255, YELLOW, 0);
        }
        for (int i = 0; i < menu_str.speed; i++)
        {
            font_print('|', font_bold, extra_width + i * px, 100, print, 255, YELLOW, 0);
        }
        char font[5] = "FONT";
        for (int i = 0; i < 5; i++)
        {
            if (i == 0)
            {
                font_print(font[i], font_bold, 32 + i * px, 140, print, 255, YELLOW, 0);
            }
            else
            {
                font_print(font[i], font_bold, 29 + i * px, 140, print, 255, YELLOW, 0);
            }
        }
        char artificial[3] = "!)";
        char artificial_bold[3] = "AI";

        for (int i = 0; i < 3; i++)
        {

            if (!font_bold)
            {
                font_print(artificial[i], font_bold, 32 + i * px, 180, print, 255, YELLOW, 0);
                if (i == 2)
                {
                    font_print(cross, font_bold, 58, 180, print, 255, 99, 113);
                }
            }
            else
            {
                font_print(artificial_bold[i], font_bold, 32 + i * px, 180, print, 255, YELLOW, 0);
                if (i == 2)
                {
                    font_print(cross, font_bold, 70, 180, print, 255, 99, 113);
                }
            }
        }
        if (menu_str.clear == false)
        {
            menu_str.refresh = false;
        }
        menu_str.clear = false;
    }
}
