#ifndef __AI_H__
#define __AI_H__

// main AI function
void ai(void);

// first direction change
void start_check(void);

// fast "normal" player algo
void fruit_hunt(void);

#endif