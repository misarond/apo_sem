#include "setup.h"
#include "utils.h"
#include <stdint.h>

// - function -----------------------------------------------------------------
void setup(void)
{
    volatile uint16_t *lcd = (volatile uint16_t *)LCD_FB_START;
    make_borders(lcd); // initialize screen borders

    char *title = "Snake";
    char *score_b = "Blue: 0";
    char *score_w = "White: 0";
    for (int i = 0; i < 5; ++i)
    {
        font_print(title[i], true, 210 + i * 12, 8, true, 255, 0, 144);
    }
    for (int i = 0; i < 7; ++i)
    {
        font_print(score_b[i], true, 64 + i * 12, 290, true, 0, 0, 255);
    }
    for (int i = 0; i < 8; ++i)
    {
        font_print(score_w[i], true, 304 + i * 12, 290, true, 255, 255, 255);
    }
    snake_game_init(&snake_player, &snake_player2, &game);

    lcd[snake_player.head_x + snake_player.head_y * LCD_WIDTH] = make_pixel(255, 255, 255);
    lcd[snake_player2.head_x + snake_player2.head_y * LCD_WIDTH] = make_pixel(0, 0, 255);
    lcd[game.fruit_x + game.fruit_y * LCD_WIDTH] = make_pixel(0, 255, 0);
}

// - function -----------------------------------------------------------------
void make_borders(volatile uint16_t *lcd)
{

    for (int i = 0; i < LCD_WIDTH * 3; i++)
    {
        lcd[i + LCD_WIDTH * 30] = make_pixel(255, 0, 0);
    }
    for (int i = 30; i < LCD_HEIGHT - 40; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            lcd[i * LCD_WIDTH + j] = make_pixel(255, 0, 0);
            lcd[i * LCD_WIDTH + (LCD_WIDTH - 1 - j)] = make_pixel(255, 0, 0);
        }
    }
    for (int i = 0; i < LCD_WIDTH * 3; i++)
    {
        lcd[(LCD_WIDTH) * (LCD_HEIGHT - 41) + i] = make_pixel(255, 0, 0);
    }
}

// - function -----------------------------------------------------------------
void snake_game_init(Snake *snake_player1, Snake *snake_player2, Game *game)
{
    snake_player1->head_x = LCD_WIDTH / 2 + 1;
    snake_player1->head_y = LCD_HEIGHT / 2;
    snake_player1->len = 1;
    snake_player1->tail_arr_x[0] = 1;
    snake_player1->tail_arr_y[0] = 1;
    snake_player1->tail_x = 0;
    snake_player1->tail_y = 0;
    snake_player1->dir = STOP;
    snake_player1->score = 0;

    snake_player2->head_x = LCD_WIDTH / 2 + 1;
    snake_player2->head_y = LCD_HEIGHT / 4 - 1;
    snake_player2->len = 1;
    snake_player2->tail_arr_x[0] = 1;
    snake_player2->tail_arr_y[0] = 1;
    snake_player2->tail_x = 0;
    snake_player2->tail_y = 0;
    snake_player2->dir = STOP;
    snake_player2->score = 0;

    game->game_over = false;
    game->level_done = false;
    game->fruit_x = 3 * (LCD_WIDTH / 4) + 1;
    game->fruit_y = LCD_HEIGHT / 2;
    game->big_font = false;
}