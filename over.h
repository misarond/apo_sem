#ifndef __OVER_H__
#define __OVER_H__

#include <stdint.h>
#include <stdbool.h>

//function prints Game over when Snake hits border or his body
void game_over(void);

//prints winner at the end
void winner(void);

#endif