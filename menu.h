#ifndef __MENU_H__
#define __MENU_H__

#include <stdint.h>
#include <stdbool.h>

typedef struct
{
    bool bold_font;
    int speed;
    bool end_menu;
    bool AI;
    int pointer;
    bool refresh;
    bool change;
    int last_print;
    bool last_click;
    bool signal;
    bool clear;
    bool speed_ref;
    bool font_change;
    bool AI_change;
    int speed_last;
} Menu;

extern Menu menu_str;

//fnction processes knob inputs
void control_menu(uint8_t inp_dir);

//function updates position of arrow in menu, clears/prints arrow
void pointer(volatile uint16_t *lcd);

//function processes click input of blue knob
void click(uint8_t inp_dir_blue);

//function takes data and gives orders to other functions (middleman)
void logic_menu();

#endif