#include "ai.h"
#include "utils.h"

void ai(void)
{
    start_check();
    fruit_hunt();
}

void start_check(void)
{
    if (snake_player2.dir == STOP)
    {
        snake_player2.dir = DOWN;
    }
}


void fruit_hunt(void)
{
    if (snake_player2.head_x > game.fruit_x && snake_player2.dir != RIGHT)
    {
        snake_player2.dir = LEFT;
    }
    else if (snake_player2.head_x > game.fruit_x)
    {
        snake_player2.dir = DOWN;
    }
    else if (snake_player2.head_x < game.fruit_x && snake_player2.dir != LEFT)
    {
        snake_player2.dir = RIGHT;
    }
    else if (snake_player2.head_x < game.fruit_x)
    {
        snake_player2.dir = UP;
    }
    else if (snake_player2.head_y > game.fruit_y && snake_player2.dir != DOWN)
    {
        snake_player2.dir = UP;
    }
    else if (snake_player2.head_y > game.fruit_y)
    {
        snake_player2.dir = LEFT;
    }
    else if (snake_player2.head_y < game.fruit_y && snake_player2.dir != UP)
    {
        snake_player2.dir = DOWN;
    }
    else if (snake_player2.head_y < game.fruit_y)
    {
        snake_player2.dir = RIGHT;
    }
}
