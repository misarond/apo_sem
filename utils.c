#include "utils.h"
#include "font_types.h"
#include <stdbool.h>
#include <stdint.h>


// - function -----------------------------------------------------------------
uint16_t make_pixel(uint8_t r, uint8_t g, uint8_t b)
{
    uint16_t px = r;
    px = (px << 6) | (g & 0x3f);
    px = (px << 5) | (b & 0x1f);
    return px;
}

// - function -----------------------------------------------------------------
void font_print(char c, bool big_font, int cord_x, int cord_y, bool print, uint8_t r, uint8_t g, uint8_t b)
{
    volatile uint16_t *lcd = (volatile uint16_t *)LCD_FB_START;
    const font_bits_t *bits;
    unsigned int height;
    int width;
    if (!big_font)
    {
        height = font_winFreeSystem14x16.height;
        width = font_winFreeSystem14x16.maxwidth;
        bits = font_winFreeSystem14x16.bits;
    }
    else
    {
        height = font_rom8x16.height;
        width = font_rom8x16.maxwidth;
        bits = font_rom8x16.bits;
    }
    int offset = ((int)c) * 16;
    for (int l = 0; l < height; l++)
    {
        uint16_t line = (uint16_t)bits[offset + l];
        for (int i = 0; i < width; i++)
        {
            int x = cord_x + i;
            int y = cord_y + l;
            uint16_t recount = 1;
            recount = recount << 15;
            recount = recount >> i;
            if ((line & recount))
            {
                if (print)
                {
                    lcd[x + y * LCD_WIDTH] = make_pixel(r, g, b);
                }
                else
                {
                    lcd[x + y * LCD_WIDTH] = make_pixel(0, 0, 0);
                }
            }
        }
    }
}
