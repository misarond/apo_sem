#ifndef __LOGIC_H__
#define __LOGIC_H__

#include <stdint.h>
#include <stdbool.h>
#include "utils.h"

// main logic of the game
void logic(int step, bool ai, Snake *snake);

// handles score change (redrawing it on lcd and on led line)
void score_change(bool player2);

// secondary fuction for score_change()
void int_to_str(int value, char *str);

// draws snake head (white) and tail (black) on lcd
void draw(void);

// updates cords of head
void head_move(Snake *snake);

// checks if a snake hasnt crashed into border
void borders_check(Snake *snake, bool player2);

// checks if a snake hasnt eaten fruit
void fruit_check(Snake *snake, bool player2, int step);

// checks if a snake hasnt crashed into itself
void hit_body_check(Snake *snake, bool player2);

// checks if a snake hasnt crashed into the other snake
void second_player_hit_check(Snake *snake, bool player2);

#endif