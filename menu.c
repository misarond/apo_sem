#include "menu.h"
#include "utils.h"
#include "font_types.h"

Menu menu_str = {
    .bold_font = false,
    .speed = 1,
    .end_menu = false,
    .AI = false,
    .pointer = 0,
    .refresh = true,
    .change = false,
    .last_print = 0,
    .last_click = true,
    .signal = false,
    .clear = false,
    .speed_ref = false,
    .font_change = false,
    .AI_change = false,
    .speed_last = 1,
};

//function - processes knob inputs
void control_menu(uint8_t inp_dir)
{
    if (inp_dir >= 0 && inp_dir < 64)
    {
        if (menu_str.pointer != 1)
        {
            menu_str.change = true;
        }
        menu_str.pointer = 1;
    }
    else if (inp_dir >= 64 && inp_dir < 128)
    {
        if (menu_str.pointer != 2)
        {
            menu_str.change = true;
        }
        menu_str.pointer = 2;
    }
    else if (inp_dir >= 128 && inp_dir < 192)
    {
        if (menu_str.pointer != 3)
        {
            menu_str.change = true;
        }
        menu_str.pointer = 3;
    }
    else if (inp_dir >= 192 && inp_dir <= 255)
    {
        if (menu_str.pointer != 4)
        {
            menu_str.change = true;
        }
        menu_str.pointer = 4;
    }
}

//function - processes knob inputs (click / blue knob)
void click(uint8_t inp_dir_blue)
{
    if (menu_str.pointer == 2 && inp_dir_blue >= 0 && inp_dir_blue <= 85 && menu_str.last_click == false)
    {
        menu_str.speed = 1;
        menu_str.signal = true;
        menu_str.last_click = true;
    }
    else if (menu_str.pointer == 2 && inp_dir_blue > 85 && inp_dir_blue <= 170 && menu_str.last_click == false)
    {
        menu_str.speed = 2;
        menu_str.signal = true;
        menu_str.last_click = true;
    }
    else if (menu_str.pointer == 2 && inp_dir_blue > 170 && inp_dir_blue <= 255 && menu_str.last_click == false)
    {
        menu_str.speed = 3;
        menu_str.signal = true;
        menu_str.last_click = true;
    }
    else if (inp_dir_blue > 100 && menu_str.last_click == false && inp_dir_blue < 130)
    {
        menu_str.signal = true;
        menu_str.last_click = true;
    }
    else if((inp_dir_blue >= 0 && inp_dir_blue <= 100) || (inp_dir_blue >= 130 && inp_dir_blue <= 255))
    {
        menu_str.signal = false;
        menu_str.last_click = false;
    }
}

void logic_menu(volatile uint16_t *lcd)
{
    uint32_t inp = *(volatile uint32_t *)(SERIAL_PORT_BASE + 4);
    char c = inp;
    if (menu_str.pointer == 1 && c == 'p')
    {
        menu_str.end_menu = true;
        menu_str.clear = true;
        menu_str.refresh = true;
    }
    else if (menu_str.pointer == 2 && menu_str.signal == true)
    {
        if (menu_str.speed_last != menu_str.speed)
        {
            menu_str.speed_last = menu_str.speed;
            menu_str.speed_ref = true;
        }
    }
    else if (menu_str.pointer == 3 && c == 'p')
    {
        menu_str.bold_font = !menu_str.bold_font;
        menu_str.clear = true;
        menu_str.refresh = true;
        menu_str.font_change = true;
    }
    else if (menu_str.pointer == 4 && c == 'p')
    {
        menu_str.AI_change = true;
        menu_str.AI = !menu_str.AI;
    }
    menu_str.signal = false;
}

//function - controls position of arrow
void pointer(volatile uint16_t *lcd)
{

    int point = '>';
    bool print = true;
    if (menu_str.end_menu)
    {
        print = false;
    }
    if (menu_str.change)
    {
        if (menu_str.last_print == 1)
        {
            font_print(point, true, 16, 60, print, 0, 0, 0);
        }
        else if (menu_str.last_print == 2)
        {
            font_print(point, true, 16, 100, print, 0, 0, 0);
        }
        else if (menu_str.last_print == 3)
        {
            font_print(point, true, 16, 140, print, 0, 0, 0);
        }
        else if (menu_str.last_print == 4)
        {
            font_print(point, true, 16, 180, print, 0, 0, 0);
        }
        menu_str.change = false;
    }
    if (menu_str.pointer == 1)
    {
        font_print(point, true, 16, 60, print, 200, ORANGE, 120);
        menu_str.last_print = 1;
    }
    else if (menu_str.pointer == 2)
    {
        font_print(point, true, 16, 100, print, 200, ORANGE, 120);
        menu_str.last_print = 2;
    }
    else if (menu_str.pointer == 3)
    {
        font_print(point, true, 16, 140, print, 200, ORANGE, 120);
        menu_str.last_print = 3;
    }
    else if (menu_str.pointer == 4)
    {
        font_print(point, true, 16, 180, print, 200, ORANGE, 120);
        menu_str.last_print = 4;
    }
}
